/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myownclone.utils;

import java.awt.Image;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;

/**
 *
 * @author tomas
 */
public class ImageFrame extends JFrame{
    
    private final Image img;
    
    public ImageFrame(Image img)
    {
        this.img = img;
    }

    public void start() {
        ImageRenderer panel = new ImageRenderer(img);
        add(panel);
        setVisible(true);
        setSize(400, 400);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
