package com.nullprogram.noise;

import com.myownclone.utils.ImageFrame;
import java.awt.Color;
import java.awt.image.BufferedImage;

/**
 * The main class for launching the application.
 */
public final class Launcher {

    /**
     * Hidden constructor.
     */
    private Launcher() {
    }

    /**
     * The main method.
     * @param args  command line arguments
     */
    public static void main(final String[] args) {
        /* Create an image. */
        Noise noise = new PerlinNoise(0, 2);
        double max = 32.0;
        double step = 0.04;
        int size = (int) (max / step);
        BufferedImage im = new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < size; i++) {
            double x = i * step;
            for (int j = 0; j < size; j++) {
                double y = j * step;
                Vector p = new Vector(x, y);
                double m = noise.sample(p);
                float v = (float) ((m + 2f / 3f) * 2f / 3f);
                int c = new Color(v, v, v).getRGB();
                im.setRGB(i, j, c);
            }
        }
        
        ImageFrame imgFrame = new ImageFrame(im);        
        imgFrame.start();
    }
}
